(ns ^:figwheel-load <%= ns-name %>.core-test
  (:require [cljs.test :refer-macros [async deftest is testing]]
            [<%= ns-name %>.core :as target]
            [spy.core :as spy]))


(deftest simple-test
  (testing "sample test."
    (is (let [x (spy/stub nil)]
          (x 1 2 3 4)
          (spy/called-with? x 1 2 3 4)))))


(deftest a-async-test
  (async done
    (js/setTimeout (fn []
                     (testing "a async test."
                       (is true)
                       (done)))
                   1000)))
