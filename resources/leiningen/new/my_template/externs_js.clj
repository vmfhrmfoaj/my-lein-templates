(ns externs-js
  (:require [clojure.string :as str]))


(println "Downloading the externs file of Chrome Extensions...")


(let [[output-file browser] *command-line-args*
      safari? (= browser "safari")
      url (if (not safari?)
            "https://raw.githubusercontent.com/google/closure-compiler/master/contrib/externs/chrome_extensions.js"
            "https://raw.githubusercontent.com/google/closure-compiler/master/contrib/externs/safari.js")
      externs (slurp url)]
  (spit output-file (cond-> externs
                      (not safari?)
                      (str/replace #"(?m)^chrome\." "browser."))))
