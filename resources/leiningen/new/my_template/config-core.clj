(ns <%= ns-name %>.config.core
  (:require [clojure.java.io :as io]
            [clojure.spec.alpha :as spec]
            [clojure.string :as str]
            [clojure.walk :as walk]
            [clj-toml.core :as toml]
            [<%= ns-name %>.config.error :as err]
            [<%= ns-name %>.config-spec :as conf-spec])
  (:import clojure.lang.ExceptionInfo))

(set! *warn-on-reflection* true)


(defn- trans-key
  "Convert the string to the keyword with replacing underbar to dash.

  Examples:
   (trans-key nil \"key_a\")
   (trans-key {\"key_a\" :key-a} \"key_a\")"
  [dict key]
  (let [s (get dict key key)]
    (keyword (str/replace s "_" "-"))))


(defn- keywordize
  "Takes a map, convert keys in the map to the keyword and return it.

  Example:
   (keywordize {\"key\" 10})"
  [dict conf]
  (walk/postwalk #(cond->> %
                    (map? %)
                    (reduce (fn [m [k v]]
                              (let [k (cond->> k
                                        (string? k) (trans-key dict))]
                                (assoc m k v)))
                            {}))
                 conf))


(defn parse-string
  "Parses config string.

  Example:
   (parse-string \"[log]\n...\")"
  ([conf-str]
   (parse-string nil conf-str))
  ([trans-dict conf-str]
   (let [raw-conf (try
                    (toml/parse-string conf-str)
                    (catch ExceptionInfo e
                      (let [{:keys [line column text input]} (ex-data e)]
                        (if-not (and line column text input)
                          (throw e)
                          (let [line (dec line)
                                start (->> input
                                           (str/split-lines)
                                           (take line)
                                           (map count)
                                           (reduce +)
                                           (+ line))
                                end (+ start column)
                                err-msg (err/err-msg-with-ctx input start end "invalid syntax")]
                            (throw (ex-info err-msg {:input [conf-str]})))))))
         md   (keywordize trans-dict (meta raw-conf))
         conf (keywordize trans-dict raw-conf)]
     (when-let [reports (some->> conf
                                 (spec/explain-data ::conf-spec/config)
                                 (::spec/problems)
                                 (map #(err/report md %)))]
       (throw (ex-info (err/summary conf-str reports) {:reports reports})))
     conf)))


(defn load-conf-file
  "Loads configuration from a file.

  Example:
   (parse-config-file \"config.example.toml\")"
  ([path]
   (load-conf-file nil path))
  ([trans-dict path]
   (when-not (and path (.exists (io/as-file path)))
     (throw (ex-info (str path " is not exists") {:input [path]})))
   (try
     (parse-string trans-dict (slurp path))
     (catch ExceptionInfo e
       (throw (ex-info (ex-message e)
                       (->> (ex-data e)
                            (:reports)
                            (map #(assoc % :file path))
                            (hash-map :problems))
                       e))))))
