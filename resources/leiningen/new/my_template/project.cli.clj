(defproject <%= proj-name %>
  "0.1.0-SNAPSHOT"

  :description "FIXME"
  :url "FIXME"
  :license {:name "The MIT License"
            :url "https://opensource.org/licenses/MIT"}

  :main <%= ns-name %>.core

  :source-paths ["src" "spec"]
  :test-paths ["test"]

  :plugins []

  :dependencies [[org.clojure/clojure "<%= clojure-ver %>"]
                 [org.clojure/tools.cli "<%= tools-cli-ver %>"]<% (when config %>
                 [com.github.vmfhrmfoaj/clj-toml "<%= clj-toml-ver %>"]<% ) %>]

  :profiles
  {:uberjar {:aot :all
             :jvm-opts ["-Dclojure.compiler.direct-linking=true"]
             ;; NOTE
             ;;  if you globally set `:target-path` to "target/%s", `lein uberjar` command will remove trampoline caches.
             :target-path "target/uberjar"}
   :dev {:source-paths ["gen" "tool"]
         :dependencies [[org.clojure/data.xml "<%= data-xml-ver %>"]
                        [org.clojure/test.check "<%= test-check-ver %>"]
                        [org.clojure/tools.deps.alpha "<%= tools-deps-alpha-ver %>" :exclusions [com.google.guava/guava]]
                        [criterium "<%= criterium-ver %>"]
                        [tortue/spy "<%= spy-ver %>"<% (when-not (platform :javascript) %> :exclusions [org.clojure/clojurescript]<% ) %>]]
         :repl-options {:init-ns user}}
   :cicd {:local-repo ".m2/repository"}
   :kaocha {:dependencies [[lambdaisland/kaocha "1.87.1366"]
                           [lambdaisland/kaocha-cloverage "1.1.89"]]}}

  :aliases {"kaocha" ["with-profile" "+kaocha" "run" "-m" "kaocha.runner"]})
