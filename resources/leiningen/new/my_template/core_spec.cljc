(ns <%= ns-name %>.core-spec
  (:require #?@(:clj  [[clojure.spec.alpha :as spec]]
                :cljs [[cljs.spec.alpha :as spec]])))


(spec/def ::simple (spec/and string? (set (map str "simple"))))
