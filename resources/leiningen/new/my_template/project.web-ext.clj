(defproject <%= proj-name %>
  "0.1.0-SNAPSHOT"

  :description "FIXME"
  :url "FIXME"
  :license {:name "The MIT License"
            :url "https://opensource.org/licenses/MIT"}

  :main <%= ns-name %>.core

  :source-paths ["src" "spec"]
  :test-paths ["test"]

  :plugins [[lein-cljsbuild "<%= lein-cljsbuild-ver %>"]]

  :dependencies [[org.clojure/clojurescript "<%= clojurescript-ver %>"]]

  :profiles
  {:dev {:source-paths ["gen" "tool"]
         :dependencies [[org.clojure/data.json "<%= data-json-ver %>"]
                        [figwheel-sidecar "<%= figwheel-sidecar-ver %>"]
                        [hiccup "<%= hiccup-ver %>"]]
         :repl-options {:init-ns user}}
   :cicd {:local-repo ".m2/repository"}
   :kaocha {:dependencies [[lambdaisland/kaocha "1.87.1366"]
                           [lambdaisland/kaocha-cloverage "1.1.89"]]}}

  :cljsbuild
  {:builds {:dev {:source-paths ["src" "tool"]
                  :compiler {:output-to "target/extension/main.js"
                             :output-dir "target/js/"
                             :externs ["target/externs.js"]
                             :closure-output-charset "US-ASCII"
                             :optimizations :whitespace
                             :pretty-print true
                             :parallel-build true}
                  :figwheel {:before-jsload dev.tool/on-compile
                             :on-jsload dev.tool/on-reload
                             ;; NOTE
                             ;;  If target site is on HTTPS, you cannot use Figwheel directly.
                             ;;  You need a proxy to convert HTTPS to HTTP.
                             ;;  See, https://github.com/bhauman/lein-figwheel/issues/365
                             ;; :websocket-url "wss://localhost:34490/figwheel-ws"
                             :websocket-host "localhost"}}
            :prod {:source-paths ["src"]
                   :compiler {:output-to "target/extension/main.js"
                              :output-dir "target/js/"
                              :externs ["target/externs.js"]
                              :closure-output-charset "US-ASCII"
                              :optimizations :advanced
                              :pretty-print false
                              :parallel-build true}}}}

  :aliases {"kaocha" ["with-profile" "+kaocha" "run" "-m" "kaocha.runner"]})
