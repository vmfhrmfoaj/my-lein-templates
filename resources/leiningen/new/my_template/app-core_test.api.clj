(ns <%= ns-name %>.app.core-test
  (:require [clojure.data.json :as json]
            [clojure.test :refer :all]
            [<%= ns-name %>.app.core :as target]
            [<%= ns-name %>.test-util :as util]
            [reitit.core :as reitit]
            [reitit.ring :as ring]
            [ring.mock.request :as mock]))

(use-fixtures :once util/without-trivial-log<% (when (#{:api-server :web-server} template) %> util/with-db<% ) %>)


(deftest echo-test
  (testing "Echo test"
    (let [route (target/routes)
          handler (util/ring-handler route)
          router (ring/router route)]
      (is (some-> router
                  (reitit/match-by-path "/api/v1/echo/foo")
                  (get-in [:result :get])))
      (is (= {"params" {"a" "10", "b" "20", "x" "bar"}}
             (some->> "/api/v1/echo/bar?a=10&b=20"
                      (mock/request :get)
                      (handler)
                      (:body)
                      (slurp)
                      (json/read-str)))))))
