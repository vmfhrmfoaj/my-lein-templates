(ns <%= ns-name %>.app.core-test
  (:require [clojure.test :refer :all]
            [<%= ns-name %>.app.core :as target]
            [<%= ns-name %>.test-util :as util]
            [reitit.core :as reitit]
            [reitit.ring :as ring]
            [ring.mock.request :as mock]))

(use-fixtures :once util/without-trivial-log<% (when (#{:api-server :web-server} template) %> util/with-db<% ) %>)


(deftest echo-test
  (testing "/index.html"
    (let [route (target/routes)
          handler (util/ring-handler route)
          router (ring/router route)]
      (is (some-> router
                  (reitit/match-by-path "/index.html")
                  (get-in [:result :get])))
      (is (string? (some->> "/index.html"
                            (mock/request :get)
                            (handler)
                            (:body)))))))
