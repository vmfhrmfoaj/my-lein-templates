(ns manifest-json
  (:refer-clojure :exclude [read-string])
  (:require [clojure.data.json :as json]
            [clojure.edn :refer [read-string]]
            [clojure.java.io :as io]
            [clojure.string :as str]))


(println "Generating manifest.json...")


(let [[output-file browser build-type] *command-line-args*
      build-type (keyword (or build-type :dev))
      [_ name version & proj] (read-string (slurp "project.clj"))
      proj (apply hash-map proj)
      js-out-file (-> proj (get-in [:cljsbuild :builds build-type :compiler :output-to]) (io/as-file) (.getName))
      manifest (cond-> {:manifest-version 2
                        :name (str name)
                        :description (:description proj)
                        :version (first (str/split version #"-"))
                        :permissions ["tabs" "storage" "http://*/*" "https://*/*"]
                        :browser-action {:default-icon "clj.png"}
                        :background {:scripts  [js-out-file "background.js"] :persistent false}
                        :content-scripts [{:js [js-out-file "content-script.js"]
                                           :run-at "document_end"
                                           :matches ["<all_urls>"]}]}
                 :always
                 (-> (update-in [:background :scripts]   #(vec (cons "polyfill.js" %)))
                     (update-in [:content-scripts 0 :js] #(vec (cons "polyfill.js" %))))

                 (= :dev build-type)
                 (assoc :content-security-policy "script-src 'self' 'unsafe-eval'; object-src 'self'")

                 (= "chrome"  browser)
                 (assoc :options-page "option.html")

                 (= "firefox" browser)
                 (assoc :options-ui {:page "option.html"}))]
  (spit output-file
        (json/write-str manifest
                        :escape-slash false
                        :key-fn #(str/replace (clojure.core/name %) #"-" "_"))))
