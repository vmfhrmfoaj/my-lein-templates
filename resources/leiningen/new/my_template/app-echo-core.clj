(ns <%= ns-name %>.app.echo.core)

(set! *warn-on-reflection* true)


(defn echo
  "Echo parameters.

  Example:
   (echo {...})"
  ([]
   (echo nil))
  ([_context]
   {:get {:summary "Echo"
          :parameters {:path {:x string?}}
          :responses {200 {:body {:params map?}}}
          :handler (fn [{:keys [params path-params body-params]}]
                     {:status 200, :body {:params (merge params path-params body-params)}})}}))
