(ns <%= ns-name %>.option.core)


(defn save []
  (.. js/browser
      -storage
      -local
      (set #js{:option (.. js/document (getElementById "opt") -checked)})))


(defn restore []
  (.. js/browser
      -storage
      -local
      (get #js{:option false}
           (fn [items]
             (set! (.. js/document (getElementById "opt") -checked)
                   (aget items "option"))))))


(defn ^:export index []
  (.. js/document (getElementById "opt") (addEventListener "change" save))
  (.addEventListener js/document "DOMContentLoaded" restore))
