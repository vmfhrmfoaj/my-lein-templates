(ns option-html
  (:require [clojure.java.io :as io]
            [hiccup.core :refer [html]]))


(println "Generating option.html...")


(let [[output-file _browser build-type] *command-line-args*
      build-type (keyword (or build-type :dev))
      proj (->> "project.clj"
                (slurp)
                (read-string)
                (drop 3)
                (apply hash-map))
      js-out-file (-> proj (get-in [:cljsbuild :builds build-type :compiler :output-to]) (io/as-file) (.getName))]
  (->> [:html [:head [:title "<%= ns-name %> options"]]
        [:body
         [:div [:input#opt {:type "checkbox"}] "Option #1"]
         [:script {:src "polyfill.js"}]
         [:script {:src js-out-file}]
         [:script {:src "option.js"}]]]
       (html)
       (spit output-file)))
