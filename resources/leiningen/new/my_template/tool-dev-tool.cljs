(ns dev.tool
  (:require [test.runner :as test]))


(defn on-compile
  "This function will be called before reloading."
  [_changed-files])


(defn on-reload
  "This function will be called after reloading."
  [changed-files]
  ;; NOTE
  ;;  This function is called when the source file is changed.
  ;;  You need to setup the `figwheel` for doing this.
  #_(apply test/run
           (->> changed-files
                (map :namespace)
                (map demunge)
                (map symbol))))
