<% (leiningen.new.my-template/import-template 0 "common_Makefile") %>
SCHEMA_FILES := $(shell find data/migrations -name '*' 2>/dev/null)
JAR_FILE     := target/uberjar/$(PROJ_NAME)-$(PROJ_VER).jar
UBERJAR_FILE := target/uberjar/$(PROJ_NAME)-$(PROJ_VER)-standalone.jar
BIN_FILE     := target/$(PROJ_NAME).bin
INSTALLED_JAR_FILE := $(HOME)/.m2/repository/$(GROUP_NAME)/$(PROJ_NAME)/$(PROJ_VER)/$(PROJ_NAME)-$(PROJ_VER).jar

.PHONY: init
init: migrate

.PHONY: migrate
migrate: .migrate

.PHONY: build
build: $(JAR_FILE)

.PHONY: test
test: migrate .test

.PHONY: isntall
install: $(INSTALLED_JAR_FILE)

.PHONY: jar
jar: $(JAR_FILE)

.PHONY: uberjar
uberjar: $(UBERJAR_FILE)

.PHONY: deploy
deploy: clean $(UBERJAR_FILE)
	$(LEIN) deploy

.PHONY: clean
clean:
	$(LEIN) clean

.PHONY: native-image
native-image: $(BIN_FILE)


pom.xml: project.clj
	$(LEIN) pom

.migrate: project.clj $(SCHEMA_FILES)
	$(LEIN) trampoline migratus migrate
	@touch .migrate

.test: project.clj $(SRC_FILES) $(RESOURCE_FILES)
	$(LEIN) trampoline kaocha
	@touch .test

$(JAR_FILE): static project.clj $(SRC_FILES) $(RESOURCE_FILES)
ifneq (install,$(MAKECMDGOALS))
	$(LEIN) jar  # `lein install` will generate a jar file, so don't need generate it here.
endif
	@test -f $@ && touch $@

$(UBERJAR_FILE): project.clj $(SRC_FILES) $(RESOURCE_FILES)
	$(LEIN) uberjar
	@test -f $@ && touch $@

$(BIN_FILE): $(UBERJAR_FILE)
#	see https://www.graalvm.org/latest/reference-manual/native-image/guides/build-static-executables/
	native-image \
		--report-unsupported-elements-at-runtime \
		--initialize-at-build-time \
		--static --libc=musl \
		--no-server \
		-jar $< \
		-H:Name=$@

$(INSTALLED_JAR_FILE): $(JAR_FILE)
	$(LEIN) install
	@test -f $@ && touch $@
