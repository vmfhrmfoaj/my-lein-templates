(ns <%= ns-name %>.app.core
  (:require [hiccup2.core :as h]))

(set! *warn-on-reflection* true)


(defn routes
  "Returns a vector that collection of app's routes.

  Examples:
   (routes)
   (routes {...})"
  ([]
   (routes nil))
  ([context]
   [["/index.html"
     {:get {:summary "Index page"
            :responses {200 {:body string?}}
            :handler (let [body (->> [:html
                                      [:head
                                       [:meta {:charset "UTF-8"}]
                                       [:meta {:name "viewport" :content "width=device-width, initial-scale=1.0"}]
                                       [:link {:rel "stylesheet" :href "/static/tailwind.css"}]
                                       [:script {:src "/static/htmx.min.js"}]
                                       [:script {:src "/static/hyperscript.min.js"}]
                                       [:script {:src "/static/extra.js"}]]
                                      [:body {:class "bg-black text-gray-200"}
                                       [:button {:hx-post "/clicked"
                                                 :hx-swap "outerHTML"}
                                        "Click Me"]]]
                                     (h/html)
                                     (str "<!DOCTYPE html>"))]
                       (fn [_param]
                         {:status 200, :body body}))}}]

    ["/clicked"
     {:post {:summary "An action of 'Click Me' button"
             :responses {200 {:body string?}}
             :handler (let [body (-> [:div "clicked"]
                                     (h/html)
                                     (str))]
                        (fn [_param]
                          {:status 200, :body body}))}}]]))
