(ns dev.repl
  (:require [clojure.java.io :as io]
            [figwheel-sidecar.build-middleware.clj-reloading :as figwheel.clj-reload]
            [figwheel-sidecar.components.cljs-autobuild :as figwheel.build]
            [figwheel-sidecar.repl-api :as figwheel]
            [figwheel-sidecar.schemas.cljs-options :as figwheel.opt]
            [figwheel-sidecar.schemas.config :as figwheel.conf]
            [strictly-specking-standalone.core :as figwheel.spec]))


(def ^:private extra-files
  "This files will be compiled after compiling CLJS files."
  (->> ["tool/test/runner_macros.clj"]
       (map io/as-file)
       (filter #(.exists %))
       (map #(.getAbsolutePath %))))


(defn- mitigate-figwheel-validation! []
  (swap! @#'figwheel.spec/registry-ref
         dissoc
         :figwheel.lein-project/figwheel
         ::figwheel.conf/build-config
         ::figwheel.opt/compiler-options)
  (figwheel.spec/def-key :figwheel.lein-project/figwheel (constantly true))
  (figwheel.spec/def-key ::figwheel.conf/build-config some?)
  (figwheel.spec/def-key ::figwheel.opt/compiler-options some?))


(defn custom-cljsbuild
  "Wraps a build function of Figwheel to run a test of the changed file."
  [{:keys [figwheel-server build-config] :as build-state}]
  (figwheel.build/cljs-build build-state)
  (figwheel.clj-reload/handle-clj-source-reloading figwheel-server build-config extra-files)
  (figwheel.build/figwheel-build build-state))


(defn start-cljs-repl
  "Starts REPL." []
  (mitigate-figwheel-validation!)
  (figwheel/start-figwheel!)
  (figwheel/cljs-repl))
