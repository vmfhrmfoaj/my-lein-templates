(ns <%= ns-name %>.content-script.core)


(defn bg-event-handler
  [option _ _]
  (let [option (js->clj option :keywordize-keys true)]
    (if (:option option)
      (set! (.. js/document
                -body
                -style
                -background)
            "red"))))


(defn ^:export index []
  (.. js/browser
      -runtime
      -onMessage
      (addListener bg-event-handler)))
