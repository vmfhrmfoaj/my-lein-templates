(ns test.runner-macros
  (:require [cljs.analyzer.api :as ana.api]
            [cljs.test :as test]))


(defmacro gen-ns-sym->test-fn
  "Stores tests into map." []
  #_(binding [*print-level* nil *print-length* nil]
      (spit (str "CLJS-compiler_" (System/currentTimeMillis) ".edn")
            (with-out-str (clojure.pprint/pprint (keys (:cljs.analyzer/namespaces @cljs.env/*compiler*))))))
  `(def ~'ns-sym->test-fn
     ~(->> (ana.api/all-ns)
           (filter #(re-find #"-test$" (name %)))
           (map #(vector `(quote ~%) `(fn [env#]
                                        (test/test-ns-block env# (quote ~%)))))
           (into {}))))


;; In REPL, test with Figwheel
(comment
  (binding [cljs.env/*compiler*
            (-> figwheel-sidecar.repl-api/*repl-api-system*
                (get-in [:figwheel-system :system])
                (deref)
                (get-in [:figwheel-server :builds "front-dev" :compiler-env]))]
    (clojure.walk/macroexpand-all '(gen-ns-sym->test-fn))))
