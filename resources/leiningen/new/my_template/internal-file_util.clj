(ns <%= ns-name %>.internal.file-util
  (:require [clojure.core.async :as async]
            [taoensso.timbre :as log])
  (:import (java.nio.file FileSystems
                          Paths
                          WatchEvent
                          WatchEvent$Kind
                          StandardWatchEventKinds)))

(set! *warn-on-reflection* true)


(defn watch-ev-kind->kw
  "Convert `WatchEvent$Kind` to keyword.

  Example:
   (watch-ev-kind->kw StandardWatchEventKinds/ENTRY_MODIFY)"
  [^WatchEvent$Kind kind]
  (condp = kind
    StandardWatchEventKinds/ENTRY_CREATE :create
    StandardWatchEventKinds/ENTRY_MODIFY :modify
    StandardWatchEventKinds/ENTRY_DELETE :delete

    (do
      (log/warn kind "is unknown event type")
      nil)))


(defn kw->watch-ev-kind
  "Convert  keyword to `WatchEvent$Kind`.

  Example:
   (kw->watch-ev-kind :modify)"
  [kw]
  (condp = kw
    :create StandardWatchEventKinds/ENTRY_CREATE
    :modify StandardWatchEventKinds/ENTRY_MODIFY
    :delete StandardWatchEventKinds/ENTRY_DELETE

    (do
      (log/warn kw "is unknown event keyword")
      nil)))


(defn watch-dir
  "Watch a file. When the file is changed notify.

  Example:
   (watch-dir \"/var/log/messages\")"
  ([dir events]
   (watch-dir dir events (async/chan)))
  ([dir events ch]
   (log/debug "start watching" (str "'" dir "'"))
   (let [path (Paths/get (str dir) (into-array String []))
         watch-srv (.newWatchService (FileSystems/getDefault))
         kinds (->> events
                    (map kw->watch-ev-kind)
                    (remove nil?)
                    (into-array WatchEvent$Kind))]
     (.register path watch-srv kinds)
     (async/go
       (loop [^WatchKey watch-key (.take watch-srv)
              [^WatchEvent ev & evs] (.pollEvents watch-key)]
         (cond
           (nil? ev)
           (do
             (.reset watch-key)
             (let [watch-key (.take watch-srv)]
               (recur watch-key (.pollEvents watch-key))))

           (async/>! ch {:root dir :rel-path (str (.context ev)) :event (watch-ev-kind->kw (.kind ev))})
           (recur watch-key evs)

           :else false))
       (log/debug "stop watching" (str "'" dir "'"))
       (.close watch-srv))
     ch)))
