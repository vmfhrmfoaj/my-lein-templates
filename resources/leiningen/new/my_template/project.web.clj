(defproject <%= proj-name %>
  "0.1.0-SNAPSHOT"

  :description "FIXME"
  :url "FIXME"
  :license {:name "The MIT License"
            :url "https://opensource.org/licenses/MIT"}

  :main <%= ns-name %>.core

  :source-paths ["src" "spec"]
  :test-paths ["test"]

  :plugins [[migratus-lein "<%= migratus-lein-ver %>"]
            [lein-cljsbuild "<%= lein-cljsbuild-ver %>"]]

  :dependencies [[org.clojure/clojure "<%= clojure-ver %>"]
                 [org.clojure/core.async "<%= core-async-ver %>"]
                 [com.fzakaria/slf4j-timbre "<%= slf4j-timbre-ver %>"]
                 [com.github.seancorfield/honeysql "<%= honeysql-ver %>"]
                 [com.github.seancorfield/next.jdbc "<%= next-jdbc-ver %>"]<% (when config %>
                 [com.github.vmfhrmfoaj/clj-toml "<%= clj-toml-ver %>"]<% ) %>
                 [com.taoensso/timbre "<%= timbre-ver %>"]
                 [environ "<%= environ-ver %>"]
                 [hiccup "<%= hiccup-ver %>"]
                 [http-kit "<%= http-kit-ver %>"]
                 [metosin/reitit "<%= reitit-ver %>"]
                 [metosin/reitit-swagger "<%= reitit-ver %>"]
                 [metosin/reitit-swagger-ui "<%= reitit-ver %>"]<% (when (= "postgresql" db) %>
                 [org.postgresql/postgresql "<%= postgresql-ver %>"]<% ) (when (= "sqlite" db) %>
                 [org.xerial/sqlite-jdbc "<%= sqlite-jdbc-ver %>"]<% ) %>
                 [ring/ring-core "<%= ring-core-ver %>"] ; required by reitit.ring.middleware
                 ]

  :profiles
  {:uberjar {:aot :all
             :jvm-opts ["-Dclojure.compiler.direct-linking=true"]
             ;; NOTE
             ;;  if you globally set `:target-path` to "target/%s", `lein uberjar` command will remove trampoline caches.
             :target-path "target/uberjar"}
   :dev {:source-paths ["gen" "tool"]
         :dependencies [[org.clojure/data.xml "<%= data-xml-ver %>"]
                        [org.clojure/test.check "<%= test-check-ver %>"]
                        [org.clojure/tools.deps.alpha "<%= tools-deps-alpha-ver %>" :exclusions [com.google.guava/guava]]
                        [org.clojure/clojurescript "<%= clojurescript-ver %>"]
                        [criterium "<%= criterium-ver %>"]
                        [haslett "<%= haslett-ver %>"]
                        [migratus "<%= migratus-ver %>"]
                        [ring/ring-mock "<%= ring-mock-ver %>"]
                        [tortue/spy "<%= spy-ver %>"<% (when-not (platform :javascript) %> :exclusions [org.clojure/clojurescript]<% ) %>]]
         :repl-options {:init-ns user
                        ;; If you got an error, remove a vector surrounding `do` expression.
                        ;; See, https://github.com/technomancy/leiningen/issues/878
                        :init [(do
                                 (require 'taoensso.timbre)
                                 (taoensso.timbre/merge-config! {:min-level :info ; if you set it to `:debug`, you may see a lot of logs.
                                                                 :timestamp-opts {:timezone (java.util.TimeZone/getDefault)}}))]}

         :migratus {:store :database
                    :migration-dir "data/migrations"
                    :db <% (leiningen.new.my-template/import-template 24 (str "migratus." db ".edn")) %>}}
   :cicd {:local-repo ".m2/repository"}
   :kaocha {:dependencies [[lambdaisland/kaocha "1.87.1366"]
                           [lambdaisland/kaocha-cloverage "1.1.89"]]}}

  :cljsbuild
  {:builds {:dev {:source-paths ["tool"]
                  :compiler {:main dev.tool
                             :asset-path "/static/out"
                             :output-dir "dev-resources/static/out"
                             :output-to  "dev-resources/static/extra.js"
                             :optimizations :none
                             :source-map-timestamp true
                             :warnings {:single-segment-namespace false}
                             :parallel-build true}}}}

  :aliases {"kaocha" ["with-profile" "+kaocha" "run" "-m" "kaocha.runner"]})
