(ns <%= ns-name %>.internal.db-util-test
  (:require [clojure.test :refer :all]
            [<%= ns-name %>.internal.db-util :as target]))


(deftest db-test
  (testing "`merge-queries` takes queries and returns a single query that merged the queries."
    (is ["SQL #1;SQL #2" 1 2 3 4 5 6]
        (target/merge-queries [["SQL #1" 1 2 3]
                               ["SQL #2" 4 5 6]])))

  (testing "The reason I created `merge-queries` is to use PostgreSQL's WITH AS syntax."
    (is [(str "WITH x AS ( INSERT INTO table VALUES (x, y) RETURING table_id ) "
              "INSERT INTO another_table VALUES (x)")]
        (target/merge-queries " " [["WITH x AS ( INSERT INTO table VALUES (x, y) RETURING table_id )"]
                                   ["INSERT INTO another_table VALUES (x)"]]))))
