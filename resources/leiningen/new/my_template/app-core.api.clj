(ns <%= ns-name %>.app.core
  (:require [<%= ns-name %>.app.echo.core :refer [echo]]
            [taoensso.timbre :as log]))

(set! *warn-on-reflection* true)


(defn routes
  "Returns a vector that collection of app's routes.

  Examples:
   (routes)
   (routes {...})"
  ([]
   (routes nil))
  ([context]
   [["/api/v1"
     ["/echo/:x" (echo context)]
     ;; add here your app routes...
     ]]))
