(ns ^{:clojure.tools.namespace.repl/load   false
      :clojure.tools.namespace.repl/unload false}
  dev.var)

(set! *warn-on-reflection* true)


(def ws-server "WebSocket server instance." (atom nil))

(def ws-ch "WebSocket channel." (atom nil))
