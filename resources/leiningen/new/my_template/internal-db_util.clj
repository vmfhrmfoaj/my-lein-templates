(ns <%= ns-name %>.internal.db-util
  (:require [taoensso.timbre :as log]))

(set! *warn-on-reflection* true)


(defn merge-queries
  "Merges queries to single query.

  Examples:
   (merge-queries [[\"INSERT INTO table VALUES (?,?,?)\" 1 2 3]
                   [\"SELECT * FROM table\"]])"
  ([queries]
   (merge-queries ";" queries))
  ([delimiter queries]
   (let [queries (cond-> queries
                   (not (sequential? queries))
                   (vector))
         [sql params] (reduce (fn [[new-sql new-params] [sql & params]]
                                [(str new-sql delimiter sql)
                                 (into new-params params)])
                              [nil []]
                              queries)
         sql (subs sql (count delimiter))]
     (into [sql] params))))
