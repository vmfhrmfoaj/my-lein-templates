(ns clojure.core.cache.extension
  (:require [clojure.core.cache :as cache]
            [clojure.core.memoize :as mem]))

(set! *warn-on-reflection* true)


(defn fifo+ttl-cache-factory
  "Returns a FIFO + TTL cache.

  Examples:
   (fifo+ttl-cache-factory {...})
   (fifo+ttl-cache-factory {...} :threshold 32 :ttl 2000)"
  [base & {:keys [threshold ttl] :or {threshold 32 ttl 2000}}]
  (mem/fifo identity
            (cache/ttl-cache-factory base :ttl ttl)
            :fifo/threshold
            threshold))
