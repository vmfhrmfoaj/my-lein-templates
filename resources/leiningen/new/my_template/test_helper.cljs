(ns test.helper
  (:require [cljs.test :as test]
            [cljs.pprint :as pp]
            [cljs.spec.alpha :as spec]
            [clojure.data :as data]))


(defn report+
  "Reports a result of test in Expections style."
  [msg]
  (try
    (when-let [[_ [pred a b]] (:actual msg)]
      (condp = (name pred)
        "="
        (let [[a' b'] (data/diff a b)
              a' (with-out-str (pp/pprint a'))
              b' (with-out-str (pp/pprint b'))]
          (println "in expected, not actual:" a')
          (println "in actual, not expected:" b'))

        "valid?" (println "explanation:" (spec/explain-str a b))))
    (catch js/Error err
      (.error js/console err))))


(let [origin (get-method test/report [::test/default :summary])]
  (defmethod test/report [::test/default :summary]
    [msg]
    (report+ msg)
    (origin msg)))
