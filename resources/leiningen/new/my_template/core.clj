(ns <%= ns-name %>.core
  (:gen-class)
  (:require [taoensso.timbre :as log])
  (:import java.util.TimeZone))

(set! *warn-on-reflection* true)


(defn -main [& _]
  (log/merge-config! {:min-level :warn, :timestamp-opts {:timezone (TimeZone/getDefault)}})

  (println "Hello, new project?"))
