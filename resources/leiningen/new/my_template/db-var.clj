(ns ^{:clojure.tools.namespace.repl/load   false
      :clojure.tools.namespace.repl/unload false}
  <%= ns-name %>.db.var)

(set! *warn-on-reflection* true)


(def conn
  "DB connection."
  (atom nil))
