# My Leniningen templates

## Usage

```
lein new my-template <PROJECT-NAME> [FLAGS] [OPTIONS]

FLAGS:
 +config: add files related to the configuration

OPTIONS:
 -type: <TYPE>

  TYPE:
   * api-server: API server
   * cli: CLI application
   * web-browser-ext: Web browser extension
   * web-server: Web server

OPTIONS(api-server, web-server):
 -db: 'sqlite', 'postgresql'. (default: 'sqlite')
```
